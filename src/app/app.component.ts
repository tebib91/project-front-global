import { Component, OnInit } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';
import { RouterOutlet, Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { slideInAnimation } from './core/animations';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [slideInAnimation]

})
export class AppComponent implements OnInit {
  title = 'projectfront';
  showHeader = false;
  showSidebar = false;
  constructor(private swUpdate: SwUpdate, private router: Router, private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.showHeader = this.activatedRoute.firstChild.snapshot.data.showHeader !== false;
        this.showSidebar = this.activatedRoute.firstChild.snapshot.data.showSidebar !== false;
      }
    });
    // Informing the user that a new version is available
    if (this.swUpdate.isEnabled) {
      this.swUpdate.available.subscribe(() => {
        if (confirm('New version available. Load New Version?')) {
          window.location.reload();
        }
      });
    }
  }

  getAnimationData(outlet: RouterOutlet) {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}

